import java.util.Scanner; // Import der Klasse Scanner
public class Eingabe_
{

 public static void main(String[] args) // Hier startet das Programm
 {

 // Neues Scanner-Objekt myScanner wird erstellt
 Scanner myScanner = new Scanner(System.in);

 System.out.print("Bitte geben Sie eine ganze Zahl ein: ");

 // Die Variable zahl1 speichert die erste Eingabe
 int zahl1 = myScanner.nextInt();

 System.out.print("Bitte geben Sie eine zweite ganze Zahl ein: ");

 // Die Variable zahl2 speichert die zweite Eingabe
 int zahl2 = myScanner.nextInt();

 // Die Addition der Variablen zahl1 und zahl2
 // wird der Variable ergebnis zugewiesen.
 int ergebnis = zahl1 + zahl2;

 System.out.print("\n\n\nErgebnis der Addition lautet: ");
 System.out.print(zahl1 + " + " + zahl2 + " = " + ergebnis);
 //Abschluss Addition
 
 //Beginn Subtraktion
 
 System.out.println("\n\nNun subtrahieren wir zwei ganze Zahlen voneinader.");
 //Das Programm fordert den Benutzer auf zwei Zahlen einzugeben
 System.out.print("\nBitte geben Sie nun den Minuend ein: ");
 // Die Variable zahl3 speichert die Eingabe
 int zahl3 = myScanner.nextInt();
 //Das Programm fordert den Benutzer auf die zweite Variable einzugeben
 System.out.print("\nBitte geben Sie nun die Subtrahend ein: ");
 // Die Variable zahl4 speichert die Eingabe
 int zahl4 = myScanner.nextInt();
 //Nun wird die Rechenoperation durchgeführt
 int ergebnis2 = zahl3 - zahl4;
 //Ausgabe des Ergebnisses
 System.out.print("\nDas Ergenis der Subtraktion lautet: ");
 System.out.print(zahl3 + "-" + zahl4 + "=" + ergebnis2);
 myScanner.close();

 }
}