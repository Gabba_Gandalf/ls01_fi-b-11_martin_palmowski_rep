import java.util.Scanner;
public class OhmschesGesetz {

	public static void main(String[] args) {
		float u;
		float r;
		float i;
		Scanner tastatur = new Scanner(System.in);
		String zeichen;
		
		System.out.print("Wollen Sie die Formel f�r die Spannung? Dann geben Sie bitte u ein f�r den Widerstand r und f�r die Stromst�rke i ");
		zeichen = tastatur.next();
		
		
		switch(zeichen) {
		case "U":
			
			System.out.println("Die Formel lautet: U = R * I");
			System.out.println("Bitte geben Sie zuerst einen Wert f�r R ein und danach einen f�r I.");	
			r = tastatur.nextFloat();
			i = tastatur.nextFloat();
			u = r*i;
			System.out.println("Die Spannung betr�gt " + u + " Volt.");
			
		break;
		case "R":
			
			System.out.println("Die Formel lautet: R = U / I");
			System.out.println("Bitte geben Sie zuerst einen Wert f�r U ein und danach einen f�r I");
			u = tastatur.nextFloat();
			i = tastatur.nextFloat();
			r = u/i;
			System.out.println("Die Spannung betr�gt " + r + " Ohm.");
			
		break;
		
		case "I":
			
			System.out.println("Die Formel lautet: I = U / R");
			System.out.println("Bitte geben Sie zuerst einen Wert f�r U ein und danach einen f�r R");
			u = tastatur.nextFloat();
			r = tastatur.nextFloat();
			i = u/r;
			System.out.println("Die Spannung betr�gt " + i + " Ampere.");
			
		break;
		
		default:
			System.out.println ("Falsche Eingabe");
			break;
			
		
		}
	}

}
