import java.util.Scanner;

public class Array {

	public static void main(String[] args) {
		
		Scanner tastatur = new Scanner(System.in);
		System.out.println("Von wie vielen Zahlen soll der Mittelwert berechnet werden?");
		int anzahl = tastatur.nextInt();
		double summe = 0;
		double[] zahlen = new double[anzahl];
		double Mittelwert = 0;
		
		// ================================
		for (int i = 0; i < zahlen.length; i++) {
			System.out.print("Bitte gebe eine Zahl f�r die Mittelwertberechnung ein: ");
			double wert = tastatur.nextDouble();
			zahlen [i] = wert;
			summe = summe + wert;
		}
		Mittelwert = summe / anzahl;
		System.out.print("Der Mittelwert lautet: " + Mittelwert );
	}

}
