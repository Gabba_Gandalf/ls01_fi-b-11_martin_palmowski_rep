import java.util.Scanner; //Klasse Scanner wird importiert
public class Abfrage {

	public static void main(String[] args) //Start des Programms
	{	
		 Scanner myScanner = new Scanner(System.in); //Scanner (myScanner) erstellt
		 
		 System.out.print("Hallo, bitte geben Sie Ihren Vornamen ein: "); //Abfrage Namen
		 String vorname = myScanner.next(); //Speicherung des Vornamens in Variable vorname
		 
		 System.out.print("Als n�chstes geben Sie bitte Ihren Nachnamen ein: "); //Abfrage Nachname
		 String nachname = myScanner.next(); //Speicherung des Nachnamens in Variable nachname
		 
		 System.out.print("Zum Schluss geben Sie noch als ganze Zahl Ihr Alter ein: "); //Abfrage Alter
		 byte alter = myScanner.nextByte(); //Speicherung des Alters in Variable alter
		 
		 //Ausgabe des Ganzen
		 System.out.print("Sie sind " + vorname +" "+ nachname + " und sind " + alter + " Jahre alt.\nEs freut mich Sie kennenzulernen!");
		 
		 
		
		
		myScanner.close();
	}

}
