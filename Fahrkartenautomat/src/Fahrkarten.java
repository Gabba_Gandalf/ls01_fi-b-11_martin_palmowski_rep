import java.util.Scanner;

class Fahrkarten
{
    public static void main(String[] args)
    {
       Scanner tastatur = new Scanner(System.in);
      
       float wunschfahrkarte;
       float zuZahlenderBetrag ; 
       float eingezahlterGesamtbetrag;
       float r�ckgabebetrag;
       boolean irgendwas = true;
       
       while (irgendwas) {
    	   // Beginn der Aufz�hlung der Methoden
    	   wunschfahrkarte = bestellvorgang (tastatur);
    	   zuZahlenderBetrag = fahrkartenbestellungErfassen (wunschfahrkarte ,tastatur);
    	   r�ckgabebetrag = fahrkartenBezahlen (zuZahlenderBetrag, tastatur);
    	   fahrkartenAusgeben();
    	   rueckgeldAusgeben (r�ckgabebetrag);
       }
       tastatur.close();
          
    }
    
    public static float bestellvorgang (Scanner myScanner) {
    	float wunschfahrkarte; 
    	Scanner tastatur = new Scanner(System.in);
    	String wahl;
    	System.out.printf("W�hlen Sie ihre Wunschfahrkarte f�r Berlin AB aus:\r\n"
    			+ "  Einzelfahrschein Regeltarif AB [2,90 EUR] (1)\r\n"
    			+ "  Tageskarte Regeltarif AB [8,60 EUR] (2)\r\n"
    			+ "  Kleingruppen-Tageskarte Regeltarif AB [23,50 EUR] (3)");
    	wahl = tastatur.next();
    	
		switch(wahl) {
		case "1":
			System.out.println("Sie haben den Einzelfahrschein f�r 2,90 � ausgew�hlt.");
			wunschfahrkarte = 2.9f;
		break;
		
		case "2":
			System.out.println("Sie haben den Regeltarif f�r 8,60 � ausgew�hlt.");
			wunschfahrkarte = 8.6f;
		break;
		
		case "3":
			System.out.println("Sie haben den Kleingruppen-Tageskarte Regeltarif AB f�r 23,50 � ausgew�hlt.");
			wunschfahrkarte = 23.5f;
		break;
		
		default:
			System.out.println ("Falsche Eingabe, bitte w�hlen Sie erneut eine Zahl.");
			wunschfahrkarte = 0f;
			break;
			
		
		}
    	
    	return wunschfahrkarte;
    }
    
    public static float fahrkartenbestellungErfassen (float wunschfahrkarte,Scanner myScanner) {
    	
    	int anzahl; 
    	float price;
    	
     
       
       System.out.printf("Wie viele Ticktes m�chten Sie?: \nMaximal 10 Tickets sind m�glich.\nW�hlen Sie mehr als 10 Tickets wird die Anzahl auf eins gesetzt.");
       anzahl = myScanner.nextInt();
       price = wunschfahrkarte * anzahl;
       
       if (anzahl > 10) {
    	   anzahl = 1;
    	   price = wunschfahrkarte * 1;  
    	   }
       
       return price;
    }
    
       
       // Geldeinwurf
       // -----------
       
     public static float fahrkartenBezahlen (float price, Scanner myScanner) {
    	
    	 float eingezahlterGesamtbetrag = 0.0f;
    	 float eingeworfeneM�nze;
    
       while(eingezahlterGesamtbetrag < price) {
    	   System.out.printf("Noch zu zahlen sind: %.2f Euro", price - eingezahlterGesamtbetrag);
    	   System.out.printf("\nEingabe (mind. 5Ct, h�chstens 2 Euro): ");
    	   eingeworfeneM�nze = myScanner.nextFloat();
           eingezahlterGesamtbetrag += eingeworfeneM�nze;
          }
       return eingezahlterGesamtbetrag - price; 
     }

       // Fahrscheinausgabe
       // -----------------
     
     public static void fahrkartenAusgeben () {
     
    	 System.out.println("Tickets werden gedruckt!");
       
    	 for (int i = 0; i < 8; i++)
       {
          System.out.print("===");
          try {
			Thread.sleep(250);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
       }
       System.out.println("\n\n");
     }

       // R�ckgeldberechnung und -Ausgabe
       // -------------------------------
     public static void rueckgeldAusgeben (float r�ckgabebetrag ) {
    	 
       if(r�ckgabebetrag > 0.0) {
    	   //double e = r�ckgabebetrag; 
    	   System.out.printf("Der R�ckgabebetrag in H�he von %.2f Euro", r�ckgabebetrag);
    	   System.out.println("\nwird in folgenden M�nzen ausgezahlt:");
    	   
   
    	   
           while(r�ckgabebetrag >= 2.0) // 2 EURO-M�nzen
           {
        	  System.out.println("2 Euro");
	          r�ckgabebetrag -= 2.0;
           }
           while(r�ckgabebetrag >= 1.0) // 1 EURO-M�nzen
           {
        	  System.out.println("1 Euro");
	          r�ckgabebetrag -= 1.0;
           }
           while(r�ckgabebetrag >= 0.5) // 50 CENT-M�nzen
           {
        	  System.out.println("50 Cent");
	          r�ckgabebetrag -= 0.5;
           }
           while(r�ckgabebetrag >= 0.2) // 20 CENT-M�nzen
           {
        	  System.out.println("20 Cent");
 	          r�ckgabebetrag -= 0.2;
           }
           while(r�ckgabebetrag >= 0.1) // 10 CENT-M�nzen
           {
        	  System.out.println("10 Cent");
	          r�ckgabebetrag -= 0.1;
           }
           while(r�ckgabebetrag >= 0.05)// 5 CENT-M�nzen
           {
        	  System.out.println("5 Cent");
 	          r�ckgabebetrag -= 0.05;
           }

       }
       System.out.println("\nVergessen Sie bitte nicht, den/die Fahrschein/e\n"+
                          "vor Fahrtantritt entwerten zu lassen!\n"+
                          "Wir w�nschen Ihnen eine gute Fahrt. \n\n");
       
       
    }
    
}